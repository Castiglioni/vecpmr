#pragma once

#include <type_traits>

namespace simplestuff
{

// Device tags
static constexpr struct is_cpu_t {} is_cpu{};
static constexpr struct is_gpu_t {} is_gpu{};

// Check device tags
template <class T>
struct is_device_tag : std::integral_constant
    <
        bool,
        std::is_same<is_cpu_t, typename std::remove_cv<T>::type>::value ||
        std::is_same<is_gpu_t, typename std::remove_cv<T>::type>::value
    > {};

// Generic template handles types that have no nested ::device_tag_t type member:
template <class, class = void>
struct has_device_tag_t : std::false_type { };

// Specialization recognizes types that do have a nested ::device_tag_t type member:
template <class T>
struct has_device_tag_t<T, std::void_t<typename T::device_tag_t>> : std::true_type {};


}
