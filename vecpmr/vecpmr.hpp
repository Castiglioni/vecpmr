#include "traits.hpp"

#ifdef __CUDACC__
#define HOST __host__
#else
#define HOST
#endif

namespace simplestuff
{
namespace details
{
// use pool tag type to enable disable init
template <class ScalarType, class DeviceType,
    typename std::enable_if<std::is_same<is_cpu_t, DeviceType>::value, bool>::type = 0
>
void vector_init(ScalarType* ptr, std::size_t size, ScalarType init_val, DeviceType)
{
    for(std::size_t i = 0; i < size; ++i)
    {
        ptr[i] = init_val;
    }
}

template <class ScalarType, class DeviceType,
    typename std::enable_if<std::is_same<is_gpu_t, DeviceType>::value, bool>::type = 0
>
void vector_init(ScalarType* ptr, std::size_t size, ScalarType init_val, DeviceType)
{
    // gpu init
}

} // namespace detail


// Very simple vector implementaion
// minimal functionality
// just enough the test pmr
template <class ScalarType, class DeviceType = is_cpu_t>
class vector
{
public:
    using device_tag_t = DeviceType;

    // ctor
    HOST vector(std::size_t size, std::pmr::memory_resource* mem_res = std::pmr::new_delete_resource())
    : _size{size}, _mem_res{mem_res}
    {
        std::size_t alignement = sizeof(ScalarType);
        std::size_t bytes = _size * alignement;
        _ptr = reinterpret_cast<ScalarType*>(_mem_res->allocate(bytes, alignement));
    }

    HOST vector(std::size_t size, ScalarType init_val, std::pmr::memory_resource* mem_res = std::pmr::new_delete_resource())
    : _size{size}, _mem_res{mem_res}
    {
        std::size_t alignement = sizeof(ScalarType);
        std::size_t bytes = _size * alignement;
        _ptr = reinterpret_cast<ScalarType*>(_mem_res->allocate(bytes, alignement));
        details::vector_init(_ptr, _size, init_val, device_tag_t{});
    }

    // dtor
    HOST ~vector()
    {
        std::size_t alignement = sizeof(ScalarType);
        std::size_t bytes = _size * alignement;
        _mem_res->deallocate(_ptr, bytes, alignement);
    }

    // minimal interface
    std::size_t size(){return _size;}
    ScalarType* data(){return _ptr;}
    ScalarType& operator[](const std::size_t i){return _ptr[i];}

private:

    ScalarType* _ptr{nullptr};
    std::size_t _size{0};
    std::pmr::memory_resource* _mem_res{nullptr};
};

}
