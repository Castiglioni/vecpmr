#pragma once

#include <memory_resource>
#include <vector>
#ifndef NDEBUG
#include <iostream>
#endif

namespace pmrext
{
namespace cuda
{

struct block
{
    void* ptr{nullptr};       // 8 bytes
    bool  is_valid{false};    // 1 byte
};

class memory_vector : public std::pmr::memory_resource
{
public:
    // ctors
    explicit memory_vector(){};
    explicit memory_vector(std::uint32_t size, std::uint32_t alignment = 64):
        _block_alignement(alignment), _block_size(size) {};
    // dtor
    ~memory_vector();

protected:

    void* do_allocate( size_t bytes, size_t alignment ) override;
    void do_deallocate( void* ptr, size_t bytes, size_t alignment ) override;
    bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override;

private:

    std::uint32_t _block_alignement{64}; // cache line aligned
    std::uint32_t _block_size{64};       // ideally multiple of cache line
    std::uint32_t _last_freed_block{};   // try to speed up search

    std::vector<block> blocks{{nullptr, false}};

};

}
}
