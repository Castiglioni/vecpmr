#pragma once

#include <memory_resource>
#include <array>
#ifndef NDEBUG
#include <iostream>
#endif

namespace pmrext
{

class memory_array : public std::pmr::memory_resource
{
public:
    // ctors
    explicit memory_array(){};
    explicit memory_array(std::uint32_t size, std::uint32_t alignment = 64):
        _block_alignement(alignment), _block_size(size) {};
    // dtor
    ~memory_array();

protected:

    void* do_allocate( size_t bytes, size_t alignment ) override;
    void do_deallocate( void* ptr, size_t bytes, size_t alignment ) override;
    bool do_is_equal(const std::pmr::memory_resource& other) const noexcept override;

private:

    std::uint32_t _block_alignement{64}; // cache line aligned
    std::uint32_t _block_size{64};       // ideally multiple of cache line
    std::uint32_t _last_freed_block{};   // try to speed up search

    static constexpr std::uint32_t _max_blocks{10};
    std::array<void*, _max_blocks> blocks{nullptr};
    std::array<bool, _max_blocks> is_used_blocks{false};

};

} // namespace pmrext
