#include "memory_vector.hpp"

namespace pmrext
{

// dtor
memory_vector::~memory_vector()
{
    // deallocate all blocks
    for(int i = 0; i < blocks.size(); ++i)
    {
        if(blocks[i].ptr != nullptr)
        {
            free(blocks[i].ptr);
#ifndef NDEBUG
            std::cout << "deallocated: " << _block_size << "\t@: " << blocks[i].ptr << '\n';
#endif
        }
    }
}

void* memory_vector::do_allocate( size_t bytes, size_t alignment )
{
    // request is bigger than pool block size
    if(bytes > _block_size)
    {
        throw std::bad_alloc{};
    }

    // search for free block
    int i = 0;
    if(!blocks[_last_freed_block].is_valid)
    {
        // we know this is free, use it
        i = _last_freed_block;
    }
    else
    {
        // otherwise look elsewhere in the pool
        for(;i < blocks.size(); ++i)
        {
            if(!blocks[i].is_valid)
            {
                break;
            }
        }
    }

    // check if we run out of blocks
    if(i == blocks.size())
    {
        blocks.push_back(block{});
    }

    // check if it was allocated before
    if(blocks[i].ptr != nullptr)
    {
        blocks[i].is_valid = true;
#ifndef NDEBUG
        std::cout << "reuse block: " << i << "\tof size: " << _block_size << '\n';
#endif
        return blocks[i].ptr;
    }

    // if not allocate
    auto ptr = aligned_alloc(_block_alignement, _block_size);
    if(!ptr)
    {
        throw std::bad_alloc{};
    }
#ifndef NDEBUG
    std::cout << "allocated: " << _block_size << "\t@: " << ptr << '\n';
#endif

    // store
    blocks[i].ptr = ptr;
    blocks[i].is_valid = true;
    return ptr;
}

void memory_vector::do_deallocate( void* ptr, size_t bytes, size_t alignment )
{
    // never deallocate, but invalidate
    for(int i = 0; i < blocks.size(); ++i)
    {
        if(blocks[i].ptr == ptr)
        {
            blocks[i].is_valid = false;
            _last_freed_block = i;
#ifndef NDEBUG
            std::cout << "freed block: " << i << "\tof size: " << _block_size <<'\n';
#endif
            return;
        }
    }
}
bool memory_vector::do_is_equal(const std::pmr::memory_resource& other) const noexcept
{
    if( this == &other ) return true;
    const auto* const op = dynamic_cast<const memory_vector*>( &other );
    return op != nullptr;
}

}
