#include "memory_array.hpp"

namespace pmrext
{

// dtor
memory_array::~memory_array()
{
    // deallocate all blocks
    for(int i = 0; i < _max_blocks; ++i)
    {
        if(blocks[i] != nullptr)
        {
            free(blocks[i]);
#ifndef NDEBUG
            std::cout << "deallocated: " << _block_size << "\t@: " << blocks[i] << '\n';
#endif
        }
    }
}

void* memory_array::do_allocate( size_t bytes, size_t alignment )
{
    // request is bigger than pool block size
    if(bytes > _block_size)
    {
        throw std::bad_alloc{};
    }

    // search for free block
    int i = 0;
    if(!is_used_blocks[_last_freed_block])
    {
        // we know this is free, use it
        i = _last_freed_block;
    }
    else
    {
        // otherwise look elsewhere in the pool
        for(;i < _max_blocks; ++i)
        {
            if(!is_used_blocks[i])
            {
                break;
            }
        }
    }

    // check if we run out of blocks
    if(i == _max_blocks)
    {
        throw std::bad_alloc{};
    }

    // check if it was allocated before
    if(blocks[i] != nullptr)
    {
        is_used_blocks[i] = true;
#ifndef NDEBUG
        std::cout << "reuse block: " << i << "\tof size: " << _block_size << '\n';
#endif
        return blocks[i];
    }

    // if not allocate
    auto ptr = aligned_alloc(_block_alignement, _block_size);
    if(!ptr)
    {
        throw std::bad_alloc{};
    }
#ifndef NDEBUG
    std::cout << "allocated: " << _block_size << "\t@: " << ptr << '\n';
#endif

    // store
    blocks[i] = ptr;
    is_used_blocks[i] = true;
    return ptr;
}

void memory_array::do_deallocate( void* ptr, size_t bytes, size_t alignment )
{
    // never deallocate, but invalidate
    for(int i = 0; i < _max_blocks; ++i)
    {
        if(blocks[i] == ptr)
        {
            is_used_blocks[i] = false;
            _last_freed_block = i;
#ifndef NDEBUG
            std::cout << "freed block: " << i << "\tof size: " << _block_size <<'\n';
#endif
            return;
        }
    }
}
bool memory_array::do_is_equal(const std::pmr::memory_resource& other) const noexcept
{
    if( this == &other ) return true;
    const auto* const op = dynamic_cast<const memory_array*>( &other );
    return op != nullptr;
}

}
