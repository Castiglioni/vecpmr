#pragma once

#include "../src/memory_array.hpp"
#include "../src/memory_vector.hpp"
#ifdef __CUDACC__
#include "../src/memory_vector.cuh"
#endif

// define version etc
