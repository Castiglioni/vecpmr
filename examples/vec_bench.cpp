// requires c++17

#include <benchmark/benchmark.h>
#include <vector>
#include <complex>
#include <iostream>

#include "memory_resource_ext.hpp"


static void arrpool(benchmark::State& state)
{
    using namespace std::pmr;

    int size = state.range(0);
    pmrext::memory_array pool(size*sizeof(double));
    int iterations = 10000;

    // time
    for (auto _ : state)
    {
        for(int i = 0; i < iterations; ++i)
        {
            {
                vector<double> vec1(size, 1.0, &pool );
                vector<double> vec2(size, 2.0, &pool );
                vector<double> vec3(size, &pool );
                // do something
                double alpha = 1.5;
                for (int j = 0; j < vec1.size(); ++j)
                {
                    vec3[j] = vec1[j] + vec2[j]*alpha;
                }

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
            // use with different tipe and size
            {
                vector<std::complex<double>> vec1(size/4, 1.0, &pool );
                vector<std::complex<double>> vec2(size/4, 2.0, &pool );
                vector<std::complex<double>> vec3(size/4, &pool );
                // do something
                std::complex<double> alpha = 1.5;
                for (int j = 0; j < vec1.size(); ++j)
                {
                    vec3[j] = vec1[j] + vec2[j]*alpha;
                }

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
        }
    }

}

BENCHMARK(arrpool)->Arg(64)->Arg(512)->Arg(1024)->Arg(2048)->Arg(1<<16)->Arg(1<<17);


static void vecpool(benchmark::State& state)
{
    using namespace std::pmr;

    int size = state.range(0);
    pmrext::memory_vector pool(size*sizeof(double));
    int iterations = 10000;

    // time
    for (auto _ : state)
    {
        for(int i = 0; i < iterations; ++i)
        {
            {
                vector<double> vec1(size, 1.0, &pool );
                vector<double> vec2(size, 2.0, &pool );
                vector<double> vec3(size, &pool );
                // do something
                double alpha = 1.5;
                for (int j = 0; j < vec1.size(); ++j)
                {
                    vec3[j] = vec1[j] + vec2[j]*alpha;
                }

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
            // use with different tipe and size
            {
                vector<std::complex<double>> vec1(size/4, 1.0, &pool );
                vector<std::complex<double>> vec2(size/4, 2.0, &pool );
                vector<std::complex<double>> vec3(size/4, &pool );
                // do something
                std::complex<double> alpha = 1.5;
                for (int j = 0; j < vec1.size(); ++j)
                {
                    vec3[j] = vec1[j] + vec2[j]*alpha;
                }

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
        }
    }

}

BENCHMARK(vecpool)->Arg(64)->Arg(512)->Arg(1024)->Arg(2048)->Arg(1<<16)->Arg(1<<17);


static void nopool(benchmark::State& state)
{
    using namespace std;

    int size = state.range(0);
    int iterations = 10000;

    // time
    for (auto _ : state)
    {
        for(int i = 0; i < iterations; ++i)
        {
            {
                vector<double> vec1(size, 1.0);
                vector<double> vec2(size, 2.0);
                vector<double> vec3(size);
                // do something
                double alpha = 1.5;
                for (int j = 0; j < vec1.size(); ++j)
                {
                    vec3[j] = vec1[j] + vec2[j]*alpha;
                }

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
            // use with different tipe and size
            {
                vector<std::complex<double>> vec1(size/4, 1.0);
                vector<std::complex<double>> vec2(size/4, 2.0);
                vector<std::complex<double>> vec3(size/4);
                // do something
                std::complex<double> alpha = 1.5;
                for (int j = 0; j < vec1.size(); ++j)
                {
                    vec3[j] = vec1[j] + vec2[j]*alpha;
                }

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
        }
    }
}

BENCHMARK(nopool)->Arg(64)->Arg(512)->Arg(1024)->Arg(2048)->Arg(1<<16)->Arg(1<<17);

BENCHMARK_MAIN();
