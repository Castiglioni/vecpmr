#include <benchmark/benchmark.h>

#include <complex>
#include <cuComplex.h>
#include <iostream>

#include "memory_resource_ext.hpp"
#include "vecpmr.hpp"

__global__
void daxpy(double a, double* x, double* y, std::size_t n, double* ret)
{
    std::size_t index = blockIdx.x * blockDim.x + threadIdx.x;
    std::size_t stride = blockDim.x * gridDim.x;
    for (std::size_t i = index; i < n; i += stride)
        if(i < n) ret[i] = a*x[i] + y[i];
}

__global__
void daxpy(std::complex<double> a, std::complex<double>* x, std::complex<double>* y,
    std::size_t n, std::complex<double>* ret)
{
    std::size_t index = blockIdx.x * blockDim.x + threadIdx.x*2;
    std::size_t stride = blockDim.x * gridDim.x;
    auto ac = make_cuDoubleComplex(
		    reinterpret_cast<double(&)[2]>(a)[0], 
		    reinterpret_cast<double(&)[2]>(a)[1]);
    for (std::size_t i = index; i < n; i += stride)
        if(i < n)
        {
            cuDoubleComplex xc = make_cuDoubleComplex(
                reinterpret_cast<double(&)[2]>(x[i])[0],
                reinterpret_cast<double(&)[2]>(x[i])[1]);
            cuDoubleComplex yc = make_cuDoubleComplex(
                reinterpret_cast<double(&)[2]>(y[i])[0],
                reinterpret_cast<double(&)[2]>(y[i])[1]);
            auto retc = cuCadd(cuCmul(ac, xc), yc);
            reinterpret_cast<double(&)[2]>(ret[i])[0] = retc.x;
            reinterpret_cast<double(&)[2]>(ret[i])[1] = retc.y;
        }
}


static void vecpool(benchmark::State& state)
{
    using namespace simplestuff;

    int size = state.range(0);
    pmrext::cuda::memory_vector pool(size*sizeof(double));
    int iterations = 10000;

    int blockSize = 256;
    int numBlocks = (size + blockSize - 1) / blockSize;

    // time
    for (auto _ : state)
    {
        for(int i = 0; i < iterations; ++i)
        {
            {
                vector<double, is_gpu_t> vec1(size, 1.0, &pool );
                vector<double, is_gpu_t> vec2(size, 2.0, &pool );
                vector<double, is_gpu_t> vec3(size, &pool );
                // do something
                double alpha = 1.5;
                daxpy<<<numBlocks, blockSize>>>(alpha, vec1.data(), vec2.data()
                    , size, vec3.data());

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
            // use with different tipe and size
            {
                vector<std::complex<double>, is_gpu_t> vec1(size/4, 1.0, &pool );
                vector<std::complex<double>, is_gpu_t> vec2(size/4, 2.0, &pool );
                vector<std::complex<double>, is_gpu_t> vec3(size/4, &pool );
                // do something
                std::complex<double> alpha = 1.5;
                daxpy<<<numBlocks, blockSize>>>(alpha, vec1.data(), vec2.data()
                    , size/4, vec3.data());

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
        }
    }

}

BENCHMARK(vecpool)->Arg(64)->Arg(512)->Arg(1024)->Arg(2048)->Arg(1<<16)->Arg(1<<17);

static void nopool(benchmark::State& state)
{
    using namespace simplestuff;

    int size = state.range(0);
    int iterations = 10000;

    // time
    for (auto _ : state)
    {
        for(int i = 0; i < iterations; ++i)
        {
            {
                vector<double> vec1(size, 1.0);
                vector<double> vec2(size, 2.0);
                vector<double> vec3(size);
                // do something
                double alpha = 1.5;
                for (int j = 0; j < vec1.size(); ++j)
                {
                    vec3[j] = vec1[j] + vec2[j]*alpha;
                }

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
            // use with different tipe and size
            {
                vector<std::complex<double>> vec1(size/4, 1.0);
                vector<std::complex<double>> vec2(size/4, 2.0);
                vector<std::complex<double>> vec3(size/4);
                // do something
                std::complex<double> alpha = 1.5;
                for (int j = 0; j < vec1.size(); ++j)
                {
                    vec3[j] = vec1[j] + vec2[j]*alpha;
                }

                benchmark::DoNotOptimize(vec3);
                benchmark::DoNotOptimize(vec2);
                benchmark::DoNotOptimize(vec1);
            }
        }
    }
}

BENCHMARK(nopool)->Arg(64)->Arg(512)->Arg(1024)->Arg(2048)->Arg(1<<16)->Arg(1<<17);

int main(int argc, char** argv)
{
   ::benchmark::Initialize(&argc, argv);
   ::benchmark::RunSpecifiedBenchmarks();
}
