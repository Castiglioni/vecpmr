# vecpmr

[![pipeline status](https://gitlab.com/Castiglioni/vecpmr/badges/main/pipeline.svg)](https://gitlab.com/Castiglioni/vecpmr/-/commits/main)


Simple array pool implemented using [polimorphic memory resources](https://en.cppreference.com/w/cpp/memory/memory_resource).

## Installation
Examples have google benchmark as dependency.
```bash
mkdir build && cd build
cmake ..
make install
```

## Usage
Few simple examples are provided.

Using pmr::vector
```bash
vec_bench
```
Using a simple custom vector implementation (representative of any custom container)
```bash
myvec_bench
```

## License
See the LICENSE file in the root directory.
